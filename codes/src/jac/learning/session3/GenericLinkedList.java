package jac.learning.session3;

public class GenericLinkedList<T> {

    private class Container{
        Container next;
        T value;
    }

    private Container start, end;
    private int size;

    public void addToFront(T value) {
        Container newNode = new Container();
        newNode.value = value;

        //size = 0
        if(size == 0){
            start = newNode;
            end = newNode;
            size++;
        }
        else{
            //size > 0
            newNode.next = start; // pointing to the ex-first element
            start = newNode;
            size++;

            //we don't pay attention to the end
        }
    }

    public void addToEnd(T value) {
        Container newNode = new Container();
        newNode.value = value;

        if(size ==0){
            start = newNode;
            end = newNode;
            size ++;
        }
        else{
            end.next = newNode;
            end = newNode;
            size++;
        }
    }


    public void add(T value){
        this.addToEnd(value);
    }

    public T get(int index) {
        //check the index => index out of bound
        if(index < 0 || index >=size){
            throw new IndexOutOfBoundsException();
        }
        //if index = size - 1
        if(index == size -1){
            return end.value;
        }
        //if index = 0
        if(index == 0){
            return start.value;
        }

        //using while loop
        Container curContainter = start;
        int counter = 0;
        while (curContainter != null){
            if (counter == index ){
                return curContainter.value;
            }
            counter ++;
            curContainter = curContainter.next;
        }
        throw new RuntimeException("Internal error");
    }
    public void insertValueAtIndex(int index, T value) {
        if(index < 0 || index > size){
            throw new IndexOutOfBoundsException();
        }

        if (size == 0 || index == size){ // either no value or at the end
            this.addToEnd(value);
            return;
        }

        if (index==0){ // insert at the beginning
            addToFront(value);
            return;
        }

        //find the container just before the position which we are going to insert
        Container before = start;
        for (int i=0; i< index - 1; i++){
            before = before.next;
        }

        Container newNode = new Container();
        newNode.value = value;

        newNode.next = before.next;
        before.next = newNode;
        size ++;

    }
    public void deleteByIndex(int index) {
        if(index < 0 || index >= size){
            throw new IndexOutOfBoundsException();
        }

        if (index == 0){
            start  = start.next;
            size --;
            return;
        }
        Container before = start;
        for (int i=0; i< index - 1; i++){
            before = before.next;
        }

        //for removing the last node
        if(index == size - 1){
            end = before;
        }
        before.next = before.next.next;
        size --;

    }
    public boolean deleteByValue(String value) {
        Container cur = start;
        int counter = 0 ;
        while (cur !=null){
            if (cur.value.equals(value)){
                deleteByIndex(counter);
                return true;
            }
            counter++;
            cur = cur.next;
        }
        return false;
    } // delete first value found

    public int getSize() { return size; }

    @Override
    public String toString() {
        Container curr = start;
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        while (curr !=null){
            builder.append(curr == start ? "" : ",");
            builder.append(curr.value);
            curr = curr.next;
        }
        builder.append("]");
        return builder.toString();
    } // comma-separated values list similar to: [5,8,11]

    public T[] toArray(T[] templateType) {
//        T[] result = new T[size];
        T[] result = (T[])java.lang.reflect.Array.newInstance(templateType.getClass().getComponentType(), size);
        Container current = start;
        for (int i = 0; i < size; i++) {
            result[i] = current.value;
            current = current.next;
        }
        return result;
    }

}
