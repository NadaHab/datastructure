package jac.learning.session3;

public class TestLinkedList {
    public static void main(String[] args) {
        LinkedListArrayOfStrings customLinkedList = new LinkedListArrayOfStrings();

        customLinkedList.addToFront("B");
        customLinkedList.addToEnd("I");
        customLinkedList.addToEnd("N");
        customLinkedList.add("G");
        customLinkedList.add("O");

        System.out.println(customLinkedList.toString());

        System.out.println(customLinkedList.get(10));

        GenericLinkedList<Long> genericLinkedList = new GenericLinkedList<>();
        genericLinkedList.addToFront(2L);

        GenericLinkedList<Float> floatGenericLinkedList = new GenericLinkedList<>();
        floatGenericLinkedList.addToFront(1f);
    }
}
