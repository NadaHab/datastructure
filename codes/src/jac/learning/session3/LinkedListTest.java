package jac.learning.session3;

import org.junit.Assert;
import org.junit.Test;

public class LinkedListTest {

    LinkedListArrayOfStrings target = new LinkedListArrayOfStrings();
    @Test
    public void testAdd(){
        target.addToFront("Peppa");
        target.addToFront("Suzy");

        Assert.assertEquals("Suzy",target.get(0));
    }

    @Test
    public void testAddByIndex(){
        target.addToFront("Peppa");
        target.addToFront("Suzy");
        target.insertValueAtIndex(1, "Danny");

        Assert.assertEquals("Danny", target.get(1));
    }

    @Test
    public void testDelete(){
        target.addToFront("Peppa");
        target.addToFront("Suzy");

        target.deleteByIndex(0);

        Assert.assertEquals(1, target.getSize());
    }
}
