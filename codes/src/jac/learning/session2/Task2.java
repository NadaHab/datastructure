package jac.learning.session2;

import java.util.Scanner;

public class Task2 {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[][] data;
        try {
            System.out.println("To enter the height and width of array");
            int height = scanner.nextInt();
            int width = scanner.nextInt();
            if (width < 1 || height < 1) {
                throw new IllegalArgumentException("The size needs to be more than 1");
            }
            data = new int[width][height];

            //give random numbers to array
            for(int row=0;row<data.length; row++){
                for (int col=0;col<data[row].length; col++){
                    data[row][col] =  -99 + (int)(Math.random() * 199);
                }
            }

            //print array
            for(int row=0;row<data.length; row++){
                for (int col=0;col<data[row].length; col++){
                    System.out.printf("%s%4d", col==0 ? "" : "," ,data[row][col]);
                }
                System.out.println();
            }

            printSumNumbers(data);
            printSumRows(data);
            printSumColumns(data);
            stdDeviation(data);
            findPairsOfPrime(data);

        }catch (IllegalArgumentException exc){
            System.out.println(exc.getMessage());
        }



    }

    private static void printSumRows(int[][] data){
        for (int row=0;row <data.length;row++){
            int sum = 0;
            for (int col=0; col<data[row].length;col++){
                sum += data[row][col];
            }
            System.out.printf("the sum of row %d is : %d", row + 1, sum);
            System.out.println();
        }

    }

    private static void printSumNumbers(int[][] data){
        System.out.println("sum of all the numbers is =>" + sumArray(data));
    }

    private static double sumArray(int[][] data){
        double sum = 0;
        for (int row=0;row <data.length;row++)
            for (int col=0; col<data[row].length;col++){
                sum += data[row][col];
            }
        return sum;
    }
    private static void printSumColumns(int[][] data){
        for(int col = 0 ;col< data[0].length; col++){
            int sum = 0;
            for (int row=0; row<data.length; row++){
                sum += data[row][col];
            }
            System.out.printf("the sum of column %d is : %d",col +1,  sum);
            System.out.println();

        }
    }

    private static void stdDeviation(int[][] data){
        double sum=0;
        double avg = sumArray(data) / (double) (data.length * data[0].length);
        for (int row = 0; row < data.length; row++) {
            for (int col = 0; col < data[row].length; col++) {
                sum += Math.pow(data[row][col] - avg, 2);
            }
        }
        double v = sum / (data.length * data[0].length) - 1;
        double std = Math.sqrt(v);
        System.out.printf("std is %.3f \n", std);
    }


    private static void findPairsOfPrime(int[][] data) {
        for (int row = 0; row < data.length; row++) {
            for (int col = 0; col < data[row].length; col++) {
                for (int row2 = row; row2 < data.length; row2++) {
                    for (int col2 = col + 1; col2 < data[row2].length; col2++) {
//                        if (row2 == row && col2 == col) {
//                            continue;
//                        }
                        int val = data[row][col] + data[row2][col2];
                        if (isPrime(val)){
                            System.out.printf("prime sum %d of [%d, %d][%d,%d]", val, row, col, row2, col2);
                        }
                    }
                }
            }
        }
    }
    private static boolean isPrime(int number){
        for(int n=2; n<=Math.sqrt(number); n++){
            if(number % n == 0){
                return false;
            }
        }

//        for(int n=2; n<=number / 2; n++){
//            if(number % n == 0){
//                return false;
//            }
//        }
        return number >= 2;
    }
}
