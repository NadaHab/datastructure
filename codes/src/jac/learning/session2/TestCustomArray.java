package jac.learning.session2;

import org.junit.Assert;
import org.junit.Test;

public class TestCustomArray {

    CustomArray target = new CustomArray();

    @Test
    public void testAddElement(){
        target.add(2);
        Assert.assertEquals( 1, target.size());
        Assert.assertEquals(2, target.get(0));
    }

    @Test
    public void testDeleteElement(){
        target.add(2);
        target.deleteByIndex(0);
        Assert.assertEquals(0, target.size());
    }

    @Test
    public void testDeleteByValue(){
        target.add(20);
        boolean actual = target.deleteByValue(20);
        Assert.assertTrue(actual);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testIndexOutOfBoundException() {
       target.get(10);
    }
}
