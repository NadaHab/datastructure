package jac.learning.session1;

public class Main {

    public static void main(String[] args) {
	    //array definition
        int[] intArray = new int[10];

        int intArray2[] = new int[20];
        for(int i=0; i< intArray2.length; i++){
            System.out.print(intArray2[i] + " ");
        }
        int[] intArray3 = new int[]{1,2,3,4,5};

        int length = intArray.length;

        //assign
        //intArray[22] = 10; // indexOutofBoundException

        intArray[5] = 10;

        long[] longArray = new long[19];

        Person[] personArray = new Person[10];
        personArray[0] = new Person("1", "211");

        for(int i=0; i< personArray.length; i++){
            System.out.print(personArray[i] + " ");
        }



        try{
            int x = 2;
            if (x == 2){
                throw new IllegalArgumentException(" it is not a good idea");
            }
        }
        catch (IllegalArgumentException exc){
            System.out.println(exc.getMessage());
        }
    }
}

class Person{
    String name;
    String tel;

    Person(String name, String tel){
        this.name = name;
        this.tel = tel;
    }
}