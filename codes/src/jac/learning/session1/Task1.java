package jac.learning.session1;

import java.util.Scanner;

public class Task1 {

    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int[] data;
        try {
            System.out.println("Enter the size of the array");

            //taking the input as the size
            int size = input.nextInt();
            //throws exception for entering wrong value
            if (size < 1) {
                throw new IllegalArgumentException("Error : Please enter a number greater than 1");
            }

            //allocate memory with the size
            data = new int[size];

            //do a loop for allocating the element values with random numbers between 1 and 100 inclusively
            for (int i = 0; i < data.length; i++) {
                data[i] = (int) (Math.random() * 100 + 1);
            }

            //print the numbers
            for (int i = 0; i < data.length; i++) {
                System.out.printf("%s %d", i == 0 ? "" : ",", data[i]);
            }

            System.out.println();
            System.out.println("prime numbers");

            //print the prime numbers
            boolean isFirst = true;
            //in this one we used enhanced for because we didn't use the index, we used the elements within the index
            for (int datum : data) {
                if (isPrime(datum)) {
                    System.out.printf("%s %d", isFirst ? "" : ",", datum);
                    isFirst = false;
                }
            }


        } catch (IllegalArgumentException exc) {
            System.out.println(exc.getMessage());
        }
    }

    //method to check if the numebr is prime or not
    private static boolean isPrime(int number) {
        for(int n=2; n<Math.sqrt(number); n++){
            if(number % n == 0){
                return false;
            }
        }
        return number >= 2;
    }
}
