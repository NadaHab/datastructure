package jac.learning.session1;

public class Array2D {
    public static void main(String[] args) {
        int[][] int2DArray = new int[3][];

        int[][] arr = {{1,2}, {3,4,5}, {6,7,8,9}};

        //System.out.println(arr[1][1]);

        for(int i = 0 ; i<arr.length; i++){
            for(int j=0; j<arr[i].length; j++){
                System.out.printf("%d ", arr[i][j]);
            }
            System.out.println();
        }

    }
}
