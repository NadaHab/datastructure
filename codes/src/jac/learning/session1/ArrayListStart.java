package jac.learning.session1;

import java.util.*;

public class ArrayListStart {

    public static void main(String[] args) {
        //Generic
        //using interface
        ArrayList<Integer> arrayList1= new ArrayList<>();
        arrayList1.add(1);

        LinkedList<Integer> linkedList = new LinkedList<>();

        int x = arrayList1.get(0);
        //arrayList.add("test"); => compile time error

        //using interface
        List<Integer> arrayList2= new ArrayList<>();

        Set<Integer> set1 = new HashSet<>();

        SortedMap<Integer, Integer> map = new TreeMap<>();


        //UPCASTING AND DOWNCASTING
        Dog dog1 = new Dog();
        Animal dog2 = new Dog();
        dog2.sound();

        Animal cat2 = new Cat();
        cat2.sound();
    }
}

interface Animal{

    void sound();
}

class Dog implements Animal{
    @Override
    public void sound(){
        System.out.println("woof");
    }
}

class Cat implements Animal{
    @Override
    public void sound(){
        System.out.println("meux");
    }
}