package jac.learning.session5;

import java.util.EmptyStackException;

public class ArrayStack {
    private int top = 0;
    private Employee[] stack;

    public ArrayStack(int capacity) {
        //initial capacity
        stack = new Employee[capacity];
    }

    public void push(Employee employee) {
        //you need to resize
        if(top == stack.length){
            Employee[] newArray = new Employee[2 * stack.length];
            System.arraycopy(stack, 0, newArray, 0, stack.length);
            stack = newArray;
        }
        stack[top++] = employee;
    }

    public Employee pop() {
        //is the stack empty?
        if(top == 0 ){
            throw new EmptyStackException();
        }
        //--top -> means the top element will be perished/disappear
        Employee employee = stack[--top];
        return employee;
    }

    public Employee peek() {
        if(top == 0 ){
            throw new EmptyStackException();
        }
        return stack[top - 1];
    }

    public int size() {
        return top;
    }

    public boolean isEmpty() {
        return top==0;
    }

    public void printStack() {
        for(int i=top - 1;i>=0; i--){
            System.out.println(stack[i]);
        }
    }
}


class Employee{
    private String firstName;
    private String lastName;
    private int id;

    public Employee(String firstName, String lastName, int id) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", id=" + id +
                '}';
    }
}