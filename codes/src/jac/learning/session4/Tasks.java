package jac.learning.session4;

public class Tasks {
    public static void main(String[] args) {

        //Fibo
        // 0, 1 , 1, 2, 3, 5, 8, 13, 21

        //we want to know the nth fibo => 80th fibo
//        for(int i=2; i< 20 ;i++)
//            System.out.println(fibo(i));

//        System.out.println(fact(4));
        System.out.println(power(2, 3));

        int array[]={89,34,7,8,10,32,40, 22};
        int max= findMax(array, array.length - 1);
        System.out.println("max is "+max);
    }

    static int fibo(int number){
        if (number == 0 ) return 0;
        if (number == 1)  return 1;
        return fibo(number - 1) + fibo( number - 2);
    }

    static int fact(int number){
        //4! = 4 * 3 * 2 * 1
        if (number ==  0 || number == 1)
            return 1;

        return number * fact( number - 1);

    }

    static int power(int base, int n){
        //2^3 = 2 ^ 2 ^ 2
        // base * power (base, n -1)
        if (n == 0) return 1;

        return base * power(base, n-1);
    }

    public static int findMax(int[] a, int index) {
        if (index == 1){
            return a[0] ;
        }

        return Math.max(a[index], findMax(a, index-1));
    }
}
