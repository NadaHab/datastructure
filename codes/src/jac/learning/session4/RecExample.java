package jac.learning.session4;

public class RecExample {
    public static void main(String[] args) {
        printFun(3);
    }

    static void printFun(int test){
        if (test<1){
            return;
        }
        System.out.printf("test is %d ", test);
        System.out.println();
        printFun(test - 1);
        System.out.println();
        System.out.printf("I come back from the stack and test is %d  ", test);
    }
}

