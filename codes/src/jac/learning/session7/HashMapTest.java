package jac.learning.session7;

import java.util.*;

public class HashMapTest {

    public static void main(String[] args) {

        Map<Integer, String> map = new HashMap<>();
        // key and value
        // zipcode => 1A1 KN2 => an address
        // () => location

        // key is unique !!!!

        map.put(1, "Peppa");
        map.put(2, "suzy");

        String value = map.get(1);

        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            System.out.println(entry.getKey() + ":" + entry.getValue());
        }

        Set<Integer> keys = map.keySet();
        for(Integer key: keys){
            String valueOfMap = map.get(key);
        }
        //hash collision

        // why we override equals and hashcode


        //HashSet
        //Tree
        //Design patterns

        //Tuesday  -> exam
    }
}

class X{
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        X x = (X) o;
        return Objects.equals(name, x.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    //they are equal or not
    //they have the same hashcode or not

    //if they are equals -> then you need to be 100% sure that they have the same hashcode
}
