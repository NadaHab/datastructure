package jac.learning.session7;

public class Complexity {
    public static void main(String[] args) {
        //finding an element in an array

        int[] array = new int[]{0,1,2,3,4};

        // O(n)
        for (int i=0; i< array.length ; i++){
            if (array[i] == 4){
                System.out.println("found");
                return;
            }
        }


        //how many steps ?
        // 5 * 4 ~ n * n
        // n ^ 2
        for (int i=0; i<array.length ;i++){
            for (int j=0; j<array.length ; j++){
                int tm = array[i];
                array[i] = array[j];
                array[j] = tm;
            }
        }

        // O (nlogn)
        // O(n^2)
    }
}
